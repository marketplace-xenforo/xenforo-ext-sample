# Information / Информация

Шаблон дополнения для [**XenForo**](https://xenforo.com/).

## Install / Установка

1. Загрузить информацию из **app/upload/**, сохраняя структуру директорий.
2. Зайти в **AdminCP**, далее *Add-ons*, и нажать кнопку <kbd>Install</kbd> у необходимого дополнения.

## Update / Обновление



## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
